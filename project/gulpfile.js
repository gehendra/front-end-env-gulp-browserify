'use strict';
var gulp = require('gulp');
var connect = require('gulp-connect');
var open = require('gulp-open');

var browserify = require('browserify');
var babel = require('gulp-babel')
var source = require('vinyl-source-stream');

var buffer = require('vinyl-buffer');

var eslint = require('gulp-eslint');

var concat = require('gulp-concat');
var minifyCSS = require('gulp-clean-css');
var scss = require('gulp-sass');

var cleanDistFolder = require('gulp-clean');


var config = {
	port: 9001,
	devBaseUrl: 'http://localhost',
	filename: {
		js: 'bundle.js',
		css: 'main.css',
		externalCSS: 'vendor.css'
	},
	paths: {
		html: './src/*.html',
		js: './src/**/*.js',
		scss: './src/**/*.scss',
		dist: './dist',
		entryPoint: './src/app.js',
		css: [
			'node_modules/bootstrap/dist/css/bootstrap.css',
			'node_modules/bootstrap/dist/css/bootstrap-theme.css'
		]
	}
};

gulp.task('connect', function() {
	connect.server({
		name: 'Web Development',
		base: config.devBaseUrl,
		root: ['dist'],
		port: config.port,
		livereload: true
	});
});

gulp.task('open', ['connect'], function() {
	gulp.src('dist/index.html')
		.pipe(open({
			uri: config.devBaseUrl + ':' + config.port + '/'
		}));
});

// clean up dist folder so the changes reflects everything server starts
gulp.task('cleanDistFolder', function() {
	cleanDistFolder(config.paths.dist);
});

gulp.task('html', function() {
	gulp.src(config.paths.html)
		.pipe(gulp.dest(config.paths.dist))
		.pipe(connect.reload());
});

gulp.task('js', function() {
		browserify(config.paths.entryPoint)
			.transform(babel)
			.bundle()
			.on('error', function() { console.error(err); this.emit('end');})
			.pipe(source(config.paths.entryPoint))
			.pipe(buffer())
			.pipe(gulp.dest(config.paths.dist + '/scripts'))
			.pipe(connect.reload());
});

gulp.task('scss', function() {
	gulp.src(config.paths.scss)
		.pipe(scss().on('error', scss.logError))
		.pipe(concat(config.filename.css))
		.pipe(minifyCSS())
		.pipe(gulp.dest(config.paths.dist + '/styles'))
		.pipe(connect.reload());
});

gulp.task('vendorCSS', function() {
	gulp.src(config.paths.css)
		.pipe(concat(config.filename.externalCSS))
		.pipe(minifyCSS())
		.pipe(gulp.dest(config.paths.dist + '/styles'))
		.pipe(connect.reload());
});

gulp.task('lint', function() {
	return gulp.src(config.paths.js)
		.pipe(eslint())
		.pipe(eslint.format())
		.pipe(eslint.failAfterError())
		.pipe(eslint.result(function(result){
			console.log('ESLint result: ' + result.filePath);
			console.log('# Messages: ' + result.messages.length);
			console.log('# Warning: ' + result.warningCount);
			console.log('# Errors: ' + result.errorCount);
		}));
});

gulp.task('watch', function() {
	gulp.watch(config.paths.html, ['html']);
	gulp.watch(config.paths.js, ['js', 'lint']);
	gulp.watch(config.paths.scss, ['scss']);
});

gulp.task('default', ['cleanDistFolder','html', 'scss', 'vendorCSS', 'js', 'lint', 'open', 'watch']);
