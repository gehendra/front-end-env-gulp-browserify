var gulp = require('gulp');
var babelify = require('bablify');
var browserify = require('browserify');

var config = {
	port: 9001,
	devBaseUrl: 'http://localhost',
	filename: {
		js: 'bundle.js',
		css: 'main.css',
		externalCSS: 'vendor.css'
	},
	paths: {
		html: './src/*.html',
		js: './src/**/*.js',
		scss: './src/**/*.scss',
		dist: './dist',
		entryPoint: './src/app.js',
		css: [
			'node_modules/bootstrap/dist/css/bootstrap.css',
			'node_modules/bootstrap/dist/css/bootstrap-theme.css'
		]
	}
};

gulp.task('js', function () {
	return gulp.src(config.paths.entryPoint)
		.pipe(babel)
		.pipe(gulp.dest(config.paths.dist))
});
